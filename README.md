Jitsi
=========

Ansible role to deploy a Jitsi Meet instance together Let's Encrypt cert.


Requirements
------------
none

Role Variables
--------------

Available variables are listed below, along with default values (see defaults/main.yml):

```
   domain: example.org
   mail: mail@example.org
```

Dependencies
------------

none

Example Playbook
----------------
    - hosts: jitsi
      roles:
         - jitsi 
